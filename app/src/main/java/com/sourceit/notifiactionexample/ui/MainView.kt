package com.sourceit.notifiactionexample.ui

interface MainView {

    fun updateText(text: String)
}