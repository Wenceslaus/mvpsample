package com.sourceit.notifiactionexample.ui

import android.os.Handler
import com.sourceit.notifiactionexample.arsenal.NotificationArsenal
import javax.inject.Inject

class MainPresenter @Inject constructor() {

    @Inject
    lateinit var notificationArsenal: NotificationArsenal

    var view: MainView? = null

    init {
        Handler().postDelayed({
            view?.updateText("after 4 sec")
        }, 4000)
    }

    fun showNotification() {
        notificationArsenal.showNewMessage()
    }
}