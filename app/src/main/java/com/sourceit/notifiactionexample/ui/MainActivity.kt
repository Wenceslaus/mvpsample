package com.sourceit.notifiactionexample.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelStoreOwner
import com.sourceit.notifiactionexample.App
import com.sourceit.notifiactionexample.R
import com.sourceit.notifiactionexample.databinding.ActivityMainBindingImpl
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var vm: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBindingImpl =
            DataBindingUtil.setContentView(
                this,
                R.layout.activity_main)
        (application as App).appComponent.inject(this)
        binding.vm = vm

        vm.showNotification()
    }

}
