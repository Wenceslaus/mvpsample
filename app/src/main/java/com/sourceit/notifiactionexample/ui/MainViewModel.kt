package com.sourceit.notifiactionexample.ui

import android.os.Handler
import androidx.databinding.ObservableField
import com.sourceit.notifiactionexample.arsenal.NotificationArsenal
import javax.inject.Inject

class MainViewModel @Inject constructor() {

    @Inject
    lateinit var notificationArsenal: NotificationArsenal

    val text = ObservableField("hello for vm")

    init {
        Handler().postDelayed({
            text.set("after 4 sec")
        }, 4000)
    }

    fun showNotification() {
        notificationArsenal.showNewMessage()
    }
}