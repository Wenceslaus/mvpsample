package com.sourceit.notifiactionexample

import android.app.Application
import com.sourceit.notifiactionexample.dagger.AppComponent
import com.sourceit.notifiactionexample.dagger.ArsenalModule
import com.sourceit.notifiactionexample.dagger.BaseModule
import com.sourceit.notifiactionexample.dagger.DaggerAppComponent

class App : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .baseModule(BaseModule(this))
            .arsenalModule(ArsenalModule())
            .build()
    }

}
