package com.sourceit.notifiactionexample.dagger

import android.content.Context
import android.os.Build
import com.sourceit.notifiactionexample.arsenal.NotificationArsenal
import com.sourceit.notifiactionexample.arsenal.NotificationArsenalOreo
import com.sourceit.notifiactionexample.arsenal.NotificationArsenalPreOreo
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ArsenalModule {

    @Singleton
    @Provides
    fun provideNotificationArsenal(context: Context): NotificationArsenal {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationArsenalOreo(context)
        } else {
            NotificationArsenalPreOreo(context)
        }
    }
}