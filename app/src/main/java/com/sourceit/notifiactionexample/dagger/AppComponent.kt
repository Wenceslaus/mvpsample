package com.sourceit.notifiactionexample.dagger

import android.content.Context
import com.sourceit.notifiactionexample.ui.MainActivity
import com.sourceit.notifiactionexample.MyReceiver
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        BaseModule::class,
        ArsenalModule::class
    ]
)

interface AppComponent {
    fun inject(mainActivity: MainActivity)


    fun getContext(): Context
    fun inject(myReceiver: MyReceiver)

}

