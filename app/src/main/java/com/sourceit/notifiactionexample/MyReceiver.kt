package com.sourceit.notifiactionexample

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.sourceit.notifiactionexample.arsenal.NotificationArsenal
import javax.inject.Inject

class MyReceiver : BroadcastReceiver() {

    @Inject
    lateinit var notificationArsenal: NotificationArsenal

    override fun onReceive(context: Context, intent: Intent) {
        (context.applicationContext as App)
            .appComponent.inject(this)

        //code
        notificationArsenal.showNewMessage()
    }
}