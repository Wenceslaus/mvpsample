package com.sourceit.notifiactionexample.arsenal

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.sourceit.notifiactionexample.arsenal.NotificationArsenal

@TargetApi(Build.VERSION_CODES.O)
class NotificationArsenalOreo(ctx: Context) : NotificationArsenal(ctx) {

    private val NOTIFICATIONS_CHANNEL_ID = "notification_channel"

    init {
        val notificationChannel = NotificationChannel(
            NOTIFICATIONS_CHANNEL_ID,
            "Notifications",
            NotificationManager.IMPORTANCE_HIGH
        )
        notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        notificationChannel.setShowBadge(true)
        notificationChannel.shouldVibrate()
        notificationChannel.vibrationPattern = vibration
        notificationChannel.enableVibration(true)
        notificationChannel.setSound(
            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION),
            AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build()
        )
        notificationManager.createNotificationChannel(notificationChannel)
    }

    override fun showNewMessage() {
        val builder = NotificationCompat.Builder(this, NOTIFICATIONS_CHANNEL_ID)
        fillNewMessage(builder)
    }

}