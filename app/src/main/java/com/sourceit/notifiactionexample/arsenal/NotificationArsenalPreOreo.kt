package com.sourceit.notifiactionexample.arsenal

import android.content.Context
import androidx.core.app.NotificationCompat
import com.sourceit.notifiactionexample.arsenal.NotificationArsenal

class NotificationArsenalPreOreo(ctx: Context) : NotificationArsenal(ctx) {

    override fun showNewMessage() {
        fillNewMessage(NotificationCompat.Builder(this))
    }

}