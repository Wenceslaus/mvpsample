package com.sourceit.notifiactionexample.arsenal

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import androidx.core.app.NotificationCompat
import com.sourceit.notifiactionexample.ui.MainActivity
import com.sourceit.notifiactionexample.R

abstract class NotificationArsenal(ctx: Context) : ContextWrapper(ctx) {

    companion object {
        const val CODE_EVENT = 110
    }

    protected var notificationManager: NotificationManager =
        (ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
    protected var vibration = longArrayOf(200, 0, 0, 200, 200, 200, 0, 0)

    abstract fun showNewMessage()

    fun show(id: Int, notification: Notification) {
        notificationManager.cancel(id)
        notificationManager.notify(id, notification)
    }

    fun fillNewMessage(builder: NotificationCompat.Builder) {
        val notifyIntent = Intent(this, MainActivity::class.java)
        builder.setStyle(NotificationCompat.BigTextStyle())
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
            .setSmallIcon(R.drawable.ic_android)
            .setAutoCancel(true)
            .setDefaults(Notification.DEFAULT_ALL)
            .setContentText("Hello, I'm notification")
            .setContentIntent(PendingIntent.getActivity(this, 0, notifyIntent, 0))
        show(CODE_EVENT, builder.build())
    }

}